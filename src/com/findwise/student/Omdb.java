package com.findwise.student;

import java.io.*;
import java.util.Map;
import java.util.Random;

/**
 * Created by Emil on 23/01/15.
 */
public class Omdb {

    JsonParser parser;
    GetPlot omdb;
    String[] movies;

    public Omdb() {
        omdb = new GetPlot();
        parser = new JsonParser();
        movies = movies();
        getNewPlot();
    }

    public static void main(String[] arg) {
        new Omdb();
    }

    public void getNewPlot() {
        int size = movies.length;
        Random rand = new Random();
        int i = rand.nextInt(size);
        for (String movie: movies) {
            writeFile(getJsonPlot(movie), movie);
        }
        System.out.println("Done");
    }

    private void writeFile(String jsonPlot, String title) {
        title = title.replaceAll("\\s+", "");
        try {
            //Whatever the file path is.
            File statText = new File("text/" + title + ".txt");
            FileOutputStream is = new FileOutputStream(statText);
            OutputStreamWriter osw = new OutputStreamWriter(is);
            Writer w = new BufferedWriter(osw);
            w.write(jsonPlot);
            w.close();
        } catch (IOException e) {
            System.err.println("Problem writing to the file statsTest.txt");
        }
    }


    public String getJsonPlot(String title) {
        return omdb.getPlot(title);
    }

    private String[] movies() {
        String[] films = {"The Matrix", "The Shawshank Redemption", "The Godfather", "The Dark Knight", "Pulp Fiction", "Fight Club", "Forrest Gump", "Inception", "Interstellar", "American History X", "Modern Times"};
        return films;
    }
}
