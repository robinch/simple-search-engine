package com.findwise.student;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

/**
 * Created by Emil on 23/01/15.
 */



public class GetPlot {
    String urlBeginning = "http://www.omdbapi.com/?t=";
    String urlEnd = "&y=&plot=full&r=json";


    public String getPlot(String title) {
        String url = plot(title);
        String json = urlReader(url);
        return json;

    }

    public String urlReader(String urlText) {
        StringBuilder json = new StringBuilder();
        InputStreamReader inStream = null;
        BufferedReader reader = null;
        URLConnection urlConn = null;
        try {
            URL url = new URL(urlText);
            urlConn = url.openConnection();
            if (urlConn != null && urlConn.getInputStream() != null) {
                inStream = new InputStreamReader(urlConn.getInputStream(), Charset.defaultCharset());
                BufferedReader bufferedReader = new BufferedReader(inStream);
                if (bufferedReader != null) {
                    int cp;
                    while ((cp = bufferedReader.read()) != -1) {
                        json.append((char) cp);
                    }
                    bufferedReader.close();
                }
            }
            inStream.close();
        } catch (Exception e) {
            throw new RuntimeException("Exception while calling URL:" + urlText, e);
        }
        return json.toString();
    }

    public String plot(String title) {
        title = title.replaceAll("\\s+", "+");

        return urlBeginning + title + urlEnd;
    }


}
