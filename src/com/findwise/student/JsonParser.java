package com.findwise.student;
import com.google.gson.Gson;
import org.json.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Robin Chowdhury on 2015-01-23.
 */
public class JsonParser {


    public Map parse(String jsonString) {

        return new Gson().fromJson(jsonString, Map.class);
    }
}
