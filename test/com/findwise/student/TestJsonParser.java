package com.findwise.student;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created by Robin Chowdhury on 2015-01-23.
 */
public class TestJsonParser {
    private JsonParser parser;
    private Map map;

    @Before
    public void init(){
        parser = new JsonParser();

    }

    @Test
    public void testParser(){
        String jsonString = "{\"Title\":\"emil har ett stort hjärta\",\"Plot\":\"Emil dör i slutet\"}";
        map = parser.parse(jsonString);
        assertEquals(map.get("Title"), "emil har ett stort hjärta");
        assertEquals(map.get("Plot"), "Emil dör i slutet");
    }

}
